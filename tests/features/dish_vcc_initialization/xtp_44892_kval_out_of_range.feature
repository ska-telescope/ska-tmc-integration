Feature: TMC throws error when the kValue is out of range

	@XTP-44892 @XTP-28347 @Team_HIMALAYA
	Scenario: TMC is able to reject command when kValue is out of range
		Given a Telescope in OFF state 
		When I issue the command LoadDishCfg on TMC with Dish and VCC configuration file   
		Then TMC rejects the command with error <error_message>
		Examples:
		    | error_message |
		    | K values are not in range (1 to 1177)|
