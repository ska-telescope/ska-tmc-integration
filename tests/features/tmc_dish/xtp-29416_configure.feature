# This BDD test performs TMC-Dish pairwise testing to verify Configure command flow.
@XTP-29416 @XTP-73796 @XTP-73795 @XTP-73596 @XTP-28347 @XTP-29778
	Scenario Outline: Configure the telescope having TMC and Dish Subsystems
		Given a Telescope consisting of TMC, DISH <dish_ids>, simulated CSP and simulated SDP
		And the Telescope is in ON state
		And TMC subarray is in IDLE obsState
		When I issue the Configure command to the TMC subarray <subarray_id>
		Then the DishMaster <dish_ids> transitions to dishMode OPERATE and pointingState TRACK
		And TMC subarray <subarray_id> obsState transitions to READY obsState
		
		    Examples:
		    | subarray_id  | dish_ids                           |
		    | 1            | SKA001,SKA036,SKA063,SKA100        |
        