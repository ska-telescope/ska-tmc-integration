# Test Documentation Index

Last updated on: 29 October 2024 12:07:20

> **NOTE**: *This file is auto-generated through a ``make bdd-steps-doc`` command. Do not edit manually*.

## Feature Files

- tests/
  - tmc_csp_new_ITH/
    - features/
      - [abort_restart_subarray.md](features/tests/tmc_csp_new_ITH/features/abort_restart_subarray.md)
      - [subarray_commands.md](features/tests/tmc_csp_new_ITH/features/subarray_commands.md)
      - [telescope_state.md](features/tests/tmc_csp_new_ITH/features/telescope_state.md)

## Step Files

- tests/
  - tmc_csp_new_ITH/
    - [conftest.md](steps/tests/tmc_csp_new_ITH/conftest.md)
    - [test_abort.md](steps/tests/tmc_csp_new_ITH/test_abort.md)
    - [test_command_triggered.md](steps/tests/tmc_csp_new_ITH/test_command_triggered.md)
    - [test_telescope_state.md](steps/tests/tmc_csp_new_ITH/test_telescope_state.md)
