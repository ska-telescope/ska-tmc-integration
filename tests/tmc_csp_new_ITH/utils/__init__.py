"""A collection of various utils temporarily kept here.

- TypedReceivedEvent: A class to store the received event data.
- TypedTangoEventTracer: A class to trace events with typed values.
- log_events: A function to log events from devices and attributes
    (with typed values).
"""
